package com.telstra.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.InjectMocks;

import com.telstra.rest.BaseTest;

public class RestServiceImplTest extends BaseTest {

	@InjectMocks
	private RestServiceImpl restServiceImpl;

	@Test
	public void testReverseWords() {
		String strRev = "how are you";
		String reverseWordActual = restServiceImpl.getReverseWords(strRev);
		assertEquals("woh era uoy", reverseWordActual);
	}

	@Test
	public void testEquilateral() {
		String str = restServiceImpl.getTraingleType(1, 1, 1).getTriangleType();
		assertEquals(RestServiceImpl.TRIANGLE.EQUILATERAL.name(), str);
	}

	@Test
	public void testIsosceles() {
		String str = restServiceImpl.getTraingleType(1, 2, 1).getTriangleType();
		assertEquals(RestServiceImpl.TRIANGLE.ISOSCELES.name(), str);
	}

	@Test
	public void testScalene() {
		String str = restServiceImpl.getTraingleType(1, 2, 3).getTriangleType();
		assertEquals(RestServiceImpl.TRIANGLE.SCALENE.name(), str);
	}

	@Test
	public void testFibonacci() {
		Long number = 10l;
		Integer fibonacciActual = restServiceImpl.getFibonaciNumber(number);
		assertEquals(new Integer(55), fibonacciActual);
	}
		
}
