package com.telstra.rest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

public class BaseTest {

	@Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
    public void test() {
    }
}
