package com.telstra.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NumberArrays {
	
	@JsonProperty("Array1")
	@NotNull(message = "error.title.notnull")
	private Integer[] array1;
	
	@JsonProperty("Array2")
    private Integer[] array2;
	
	@JsonProperty("Array3")
	@Valid
	
    private Integer[] array3;
	
	public Integer[] getArray1() {
		return array1;
	}

	public void setArray1(Integer[] array1) {
		this.array1 = array1;
	}

	public Integer[] getArray2() {
		return array2;
	}

	public void setArray2(Integer[] array2) {
		this.array2 = array2;
	}

	public Integer[] getArray3() {
		return array3;
	}

	public void setArray3(Integer[] array3) {
		this.array3 = array3;
	}

	
}
