package com.telstra.service;

import com.telstra.model.NumberArrays;
import com.telstra.model.SingleArray;
import com.telstra.model.TriangleType;

public interface RestService {
	
	 SingleArray makeOneArray(NumberArrays numberArrs);
	 
	 TriangleType getTraingleType(Integer a,Integer b,Integer c);
	 
	 String getReverseWords(String sentence);
	 
	 Integer getFibonaciNumber(Long number);

}
