package com.telstra.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.telstra.model.NumberArrays;
import com.telstra.model.SingleArray;
import com.telstra.model.TriangleType;
import com.telstra.service.RestService;

@Component
public class RestServiceImpl implements RestService {

	public enum TRIANGLE {
		EQUILATERAL, ISOSCELES, SCALENE
	}

	@Override
	public Integer getFibonaciNumber(Long number) {
		int a = 0;
		int b = 1;
		try {
			if (number >= 1 && number != null) {

				for (int i = 1; i <= number; i++) {
					int temp = a;
					a = a + b;
					b = temp;
				}
			}

		} catch (NumberFormatException e) {
			e.getMessage();
		}

		return a;
	}

	@Override
	public String getReverseWords(String sentence) {
		StringBuilder revBuilder = null;
		String resultReverse = "";
		if (sentence != null && !sentence.isEmpty()) {
			Pattern pattern = Pattern.compile("\\s");
			String[] temp = pattern.split(sentence);
			for (int i = 0; i < temp.length; i++) {
				if (i == temp.length - 1)
					resultReverse = temp[i] + resultReverse;
				else
					resultReverse = " " + temp[i] + resultReverse;
			}
			revBuilder = new StringBuilder(resultReverse);
		}
		return revBuilder.reverse().toString();

	}

	@Override
	public TriangleType getTraingleType(Integer a, Integer b, Integer c) {
		if (a <= 0 || b <= 0 || c <= 0) {
			throw new IllegalArgumentException("Triangle sides should be greater than zero");
		}

		TriangleType triangleType = new TriangleType();
		if (a == b && b == c) {
			triangleType.setTriangleType(TRIANGLE.EQUILATERAL.name());
			return triangleType;
		}

		else if ((a == b) || (c == a) || (c == b)) {
			triangleType.setTriangleType(TRIANGLE.ISOSCELES.name());
			return triangleType;
		}

		else {
			triangleType.setTriangleType(TRIANGLE.SCALENE.name());
			return triangleType;
		}

	}

	@Override
	public SingleArray makeOneArray(NumberArrays numberArrs) {
		Integer[] array1 = numberArrs.getArray1();
		Integer[] array2 = numberArrs.getArray2();
		Integer[] array3 = numberArrs.getArray3();

		List<Integer> listWithDuplicates = new ArrayList<>(Arrays.asList(array1));
		listWithDuplicates.addAll(Arrays.asList(array2));
		listWithDuplicates.addAll(Arrays.asList(array3));

		// Removing duplicates from the list using Java 8 Streams
		List<Integer> listWithoutDuplicates = listWithDuplicates.stream().distinct().sorted()
				.collect(Collectors.toList());
		SingleArray singleArray = new SingleArray();
		singleArray.setArray(listWithoutDuplicates.toArray());
		return singleArray;
	}

}
